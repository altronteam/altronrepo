-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 30, 2017 at 10:51 AM
-- Server version: 10.0.30-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `altrondi_power`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(20) NOT NULL,
  `productID` int(20) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `productID`, `image`) VALUES
(40, 25, 'product161205114842.jpg'),
(41, 25, 'product161205114852.jpg'),
(42, 26, 'product161205115832.jpg'),
(43, 26, 'product161205115845.jpg'),
(44, 26, 'product161205115854.jpg'),
(45, 26, 'product161205115907.jpg'),
(46, 26, 'product161205115918.jpg'),
(47, 27, 'product161205120509.jpg'),
(48, 28, 'product161205121232.jpg'),
(49, 28, 'product161205121249.jpg'),
(50, 28, 'product161205121301.jpg'),
(51, 28, 'product161205121312.jpg'),
(52, 29, 'product161205121917.jpg'),
(53, 29, 'product161205121932.jpg'),
(54, 29, 'product161205121941.jpg'),
(55, 29, 'product161205121952.jpg'),
(56, 30, 'product161205122313.jpg'),
(57, 30, 'product161205122325.jpg'),
(58, 31, 'product161205123142.jpg'),
(59, 31, 'product161205123152.jpg'),
(60, 31, 'product161205123201.jpg'),
(61, 32, 'product161205123455.jpg'),
(62, 32, 'product161205123505.jpg'),
(63, 33, 'product161205124601.jpg'),
(64, 33, 'product161205124612.jpg'),
(65, 34, 'product161205125141.jpg'),
(66, 34, 'product161205125202.jpg'),
(67, 35, 'product161205125907.jpg'),
(69, 35, 'product161205125933.jpg'),
(70, 35, 'product161205125944.jpg'),
(73, 37, 'product161205010625.jpg'),
(74, 38, 'product161205010637.jpg'),
(75, 39, 'product161205010648.jpg'),
(76, 36, 'product161205010907.jpg'),
(79, 27, 'booking.php');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(20) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`) VALUES
(1, 'admin', '46b05b3b2ad45ecbdd40e4629e7efccb');

-- --------------------------------------------------------

--
-- Table structure for table `productcategory`
--

CREATE TABLE `productcategory` (
  `id` int(20) NOT NULL,
  `categoryName` varchar(2000) NOT NULL,
  `description` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productcategory`
--

INSERT INTO `productcategory` (`id`, `categoryName`, `description`) VALUES
(1, 'UPS', 'An uninterruptible power supply(UPS) provides emergency power to a load when input mains power fails. A UPS provide near-instantaneous          protection for critical devices such as computers, medical equipments, telecommunication equipments etc. from input power interruptions .         We have the following range of solutions under this category.'),
(2, 'HOME UPS', 'Altron HOME UPS series are specially designed for household appliances.  The latest DSP based technology ensures you an uninterruptible output during power          failures. Best technology makes it suitable to use in Indian conditions. We have the following range of HOME UPS models meeting diverse needs of the customers'),
(3, 'SOLAR PCU', 'Solar Power Conditioning Unit (SOLAR PCU) consists inverter,grid charger and an integrated solar charge controller. It has provision to charge the battery          bank either through Solar or Grid/DG Set. We have the following ranges of solar solutions  to meet all kinds of solar needs.'),
(4, 'SOLAR CHARGE CONTROLLERS', 'Our range of solar charge controllers are designed with intelligent load / Dusk to dawn control options makes it ideal for street and home lighting applications.           We offer its following variants as per customer requirements.'),
(5, 'CONTROL CARDS', 'We do support with Control cards in the range 650VA to 2500VA.'),
(6, 'TRANSFORMERS', 'We offer a wide range of transformers as per customer specifications in terms of size,rating and efficiency.'),
(7, 'SOLAR PV PANEL', 'Altron supplies an extensive range of superior quality solar panels from reputed manufactures. We are authorized dealers for Vikram Solar and SOVA Solar'),
(8, 'BATTERIES', 'Altron is having authorized distributionship with Southern Batteries( Hi-Power) and Authorized dealership with Exide Industries Limited(EXIDE) Batteries. We offer batteries ranging from 7Ah to 200Ah.');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(20) NOT NULL,
  `categoryID` int(20) NOT NULL,
  `productName` varchar(200) NOT NULL,
  `productDescription` varchar(2000) DEFAULT NULL,
  `brochure` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `categoryID`, `productName`, `productDescription`, `brochure`) VALUES
(25, 1, 'Line interactive UPS-Fusion(F) Series(300VA-2500VA)', '<p>. High efficiency and reliability</p>\r\n<p>. Pure sinewave output</p>\r\n<p>. Multiple information display panel</p>\r\n<p>. Over load and Short circuit protection</p>', 'BR161208111018.pdf'),
(26, 1, 'Line interactive UPS-Unity(U) Series(2KVA-5KVA)', '<p>. Digital Signal Processor control</p>\r\n<p>. Extended backup time</p>\r\n<p>. User friendly LCD/LED display</p>\r\n<p>. Instant switchover time</p>', 'BR161208111712.pdf'),
(27, 1, 'Online UPS-Aerate(A) series(1KVA-30KVA)', '<p>. User friendly LCD/LED display</p>\r\n<p>. DSP based PWM technology using MOSFET\'s</p>\r\n<p>. Battery deep discharge and overcharge protection</p>\r\n<p>. Smart overload and short circuit protection</p>', ''),
(28, 2, 'Sinewave HOME UPS-Junior(J) series(500VA to 30KVA)', '<p>. High cold start capacity</p>\r\n<p>. Ideal for small power backup needs</p>\r\n<p>. Intelligent thermal management</p>\r\n<p>. Automatic temperature compensation</p>', ''),
(29, 3, 'MOSFET PCU with PWM charger-Kinetic(K) series(500VA to 2.5KVA)', '<p>. Extended backup time</p>\r\n<p>. User friendly LCD/LED display</p>\r\n<p>. Instant switchover time</p>\r\n<p>. DSP based PWM technology using MOSFET\'s</p>', ''),
(30, 3, 'MOSFET PCU with MPPT charger-Micron(M) Series(100VA to 6KVA)', '<p>. Regulated output voltage</p>\r\n<p>. Customized battery charging facility</p>\r\n<p>. Over temperature protection</p>\r\n<p>. Solar compatibility</p>', ''),
(31, 3, 'IGBT PCU( 1 Ph/3 Ph) with MPPT charger-Vacuum(V) Series(5KVA to 50KVA)', '<p>. Smart overload and short circuit protection with autorestart</p>\r\n<p>. Reverse current flow protection for PV</p>\r\n<p>. Efficient &amp; faster charging</p>\r\n<p>. High cold start capacity</p>', ''),
(32, 4, 'Solar PWM Charge controllers-Quark(Q) Series(5A to 40A)', '<p>. Solar compatibility</p>\r\n<p>. Reverse battery protection</p>\r\n<p>. Microprocessor control</p>\r\n<p>. Over load and Short circuit protection</p>', ''),
(33, 4, 'Solar MPPT charge controllers- Zeero(Z) series(5A to 100A)', '<p>. Battery deep discharge and overcharge protection</p>\r\n<p>. 3 Phase interleaved switching&nbsp;</p>\r\n<p>. Solar reverse polarity protection</p>\r\n<p>. Reverse current flow protection</p>', ''),
(34, 4, 'Solar MPPT charge controllers- Green(G) series(5A to 100A)', '<p>. User friendly &nbsp;LED/LCD display</p>\r\n<p>. Battery deep discharge and overcharge protection</p>\r\n<p>. 3 Phase interleaved switching&nbsp;</p>\r\n<p>. Solar reverse polarity protection</p>', ''),
(35, 5, 'Digital HOME UPS/ Inverter', '', ''),
(36, 5, 'Sinewave home UPS with DSP technology', '', ''),
(37, 6, 'Inverter/UPS Transformers', '', ''),
(38, 6, 'Safety isolation transformers (1Ph/3Ph)', '', ''),
(39, 6, 'K rated transformers', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productcategory`
--
ALTER TABLE `productcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `productcategory`
--
ALTER TABLE `productcategory`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
