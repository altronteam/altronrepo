    
    <!-- Footer -->
    <div class="footer">
        <div class="footer_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer_logo_area">
                            <a href="index.php"><img src="images/logo.png" alt="Altron" /></a>
                            <div class="footer_soc">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </div>
                        </div>
                        <div class="footer_contact">
                            <div class="footer_contact_block">
                                <span i class="fa fa-map-marker"></span>
                                <b>ALTRON DIGITAL SYSTEMS Pvt. Ltd.</b><br>
                                17/249F, Platinum Tower, Pavamani Road<br> Calicut-673 001
                            </div>
                            <div class="footer_contact_block">
                                <span i class="fa fa-phone"></span>
                                <a href="tel:04953204426">0495-3204426</a>&nbsp;&nbsp;<a href="tel:04954099096">0495-4099096</a>&nbsp;&nbsp;<a href="tel:04952722698"> 0495-2722698</a>
                            </div>
                            <div class="footer_contact_block">
                                <span i class="fa fa-mobile"></span>
                                <a href="tel:+919037088828">+9190370 88828</a>&nbsp;&nbsp;<a href="tel:+919388870466">+9193888 70466</a>
                            </div>
                            <div class="footer_contact_block">
                                <span i class="fa fa-at"></span>
                                <a href="mailto:info@altron.com">info@altron.com, altrondigitalsystems@gmail.com</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copy_wrap">
                            <span class="copy">&copy; Altron All rights reserved</span>
                            <span class="power">Powered by <a href="http://www.bodhiinfo.com" target="_blank"><img src="images/bodhi_logo.png" alt="Bodhi" /></a></span>
                            <div class="bd_clear"></div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>        
    </div>   
    <?php if($page_id==3){ ?>
    <script src="js/slick.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $(document).on('ready', function () {
            $(".regular").slick({
                autoplay: true
                , dots: true
                , infinite: true
                , slidesToShow: 1
                , slidesToScroll: 1
            });
        });
    </script>
	<script src="light/lightgallery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
    <script src="light/lg-fullscreen.min.js"></script>
    <script src="light/lg-thumbnail.min.js"></script>
    <script>
        jQuery(document).ready(function($){
		  //you can now use $ as your jQuery object.
		  	$('.product_page_wrap_inner').lightGallery({
    			download: true,
                thumbnail: false,
                fullScreen: false,
                selector: '.product_image a'
    		});
		});
    </script>
    <?php } ?>
</body>
</html>
