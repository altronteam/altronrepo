/*jslint browser: true*/
/*global $, jQuery, alert*/
$(function () {
    'use strict';
    // Banner
    $('.banner').bxSlider({
        pager: false,
        controls: false,
        auto: true,
        easing: 'ease-in-out',
        touchEnabled: false,
        onSliderLoad: function () {
            $('.banner>li .cap_block').eq(1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");
            
        },
        onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
            //console.log(currentSlideHtmlObject);
            $('.active-slide').removeClass('active-slide');
            $('.banner>li .cap_block').eq(currentSlideHtmlObject + 1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");

        },
        onSlideBefore: function () {
            $(".cap_block.active-slide").removeClass("wow animated fadeInUp");
            $(".one.cap_block.active-slide").removeAttr('style');
            
        }
    });
    // Testimonial
    $('.testimonial').bxSlider({
        controls: false,
        auto: true
    });
    // Navigation
    $('.nav_trigger').click(function () {
        $('.navigation').slideToggle();    
    });
    $(window).resize(function(){
        var windWidth = $(window).width();
        if(windWidth >= 768)
        {
            $('.navigation').show();
            $('.res_navigation').hide();
        }
        else
        {
            $('.navigation').hide();       
        }
    });
    $('.nav_trigger_inner').click(function(){
        $('.res_navigation').slideToggle();
    });
    
    /*$('.contact_form').submit(function(e){
        e.preventDefault();
        checkVal();
    });
    function checkVal(){
        var proceed = false;
        if( $(".contact_form")[0].checkValidity ){
            if ($(".contact_form")[0].checkValidity()) {
                proceed = true;
            }
        }else{
            proceed = true;
        }
        if (proceed) {
            //Do ajax call
            var data = $('.contact_form').serializeArray();
            //console.log(data);
            $.post('mail.php', data, function(data){
                //console.log(data.status);
                if(data.status){
                    alert('Thank you for your feedback.');
                    $('.valid_entry').val('');
                } else {
                    alert('Oops! Something went wrong. Please try again later.');
                }
            }, 'json');
        }
    }*/
    
    
    // Product Navigation
    $(document).on('click', '.product_nav ul > li > a', function(e){
        e.preventDefault();
        var curProNav = $(this),
            curProTarget = $(curProNav.attr('href'));
        //console.log(curProTarget);
        $('.product_nav ul > li > a').not(curProNav).removeClass('active');
        curProNav.addClass('active');
        $('.product_row').not(curProTarget).removeClass('active');
        curProTarget.addClass('active');
    });
    
    
    
    
    
    
    
    
    
    
    
    
});