<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID'])==''||($_SESSION['LogType']!='admin'))
{
header("location:../../logout.php");
}
$loginId	=	$_SESSION['LogID'];
//$loginType	=	$_SESSION['LogType'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] :$_GET['op']) : $_POST['op'];
switch($optype)
{
// NEW SECTION
case 'index':
			if(!$_REQUEST['productName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
			$db->connect();
			$success2					=	0;
				
			$data['productName'] 	 	=	$App->convert($_REQUEST['productName']);		
			$data['categoryID']		 	=	$App->convert($_REQUEST['category']);
			$data['productDescription']	=	$App->convert($_REQUEST['description']);
			//brochure upload						
			
			// Allow certain file formats			
			if(is_uploaded_file($_FILES['brochure']['tmp_name'])) { 
				$date			= date("YmdHis");
				$newfilename 	= $date.".pdf";
				$target_file 	= "brochure/$newfilename";
				$maxsize	 	= 8388608;
				$imageFileType  = pathinfo($target_file,PATHINFO_EXTENSION);
				
				if($imageFileType != "pdf") {
				    $_SESSION['msg']	=	 "Sorry, Only Pdf files are allowed.";
				    $success2			=	1;
				    header("location:index.php");
				}
				else if(($_FILES['brochure']['size'] >= $maxsize)) 	    {
	        		$_SESSION['msg']	=	 "Sorry, File too large. File must be less than 8 megabytes.";
	        		$success2			=	1;
				    header("location:index.php");
	    		}
				else{
					$res=move_uploaded_file($_FILES["brochure"]["tmp_name"], $target_file);
					if ($res) {
						$data['brochure']	=	$newfilename;
						$success1			=	$db->query_insert(TABLE_PRODUCTS,$data);	     
				    }
				}
			}
			else
			{
				$data['brochure']	=	"";
				$success1			=	$db->query_insert(TABLE_PRODUCTS,$data);
			}
							
			$db->close();			
			if($success1)
			{							
				$_SESSION['msg']="New Product Added Successfully";											$success2			=	1;
			}
			else if($success2==0)
			{							
				$_SESSION['msg']="Failed";										
			}			
			header("location:index.php");
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];
		if(!$_REQUEST['productName'])
		{
			$_SESSION['msg']="Error, Invalid Details!";					
			header("location:index.php");		
		}
		else
		{				
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
			$db->connect();
			$success2					=	0;
				
			$data['productName'] 	 	=	$App->convert($_REQUEST['productName']);		
			$data['categoryID']		 	=	$App->convert($_REQUEST['category']);
			$data['productDescription']	=	$App->convert($_REQUEST['description']);
			//brochure upload									
			// Allow certain file formats
			if(is_uploaded_file($_FILES['brochure']['tmp_name'])) {   
				$date			= date("YmdHis");
				$newfilename 	= $date.".pdf";
				$target_file 	= "brochure/$newfilename";
				$maxsize	 	= 8388608;
				$imageFileType  = pathinfo($target_file,PATHINFO_EXTENSION);
				
				if($imageFileType != "pdf") {
				    $_SESSION['msg']	=	 "Sorry, Only Pdf files are allowed.";
				    $success2			=	1;
				    header("location:index.php");
				}
				else if(($_FILES['brochure']['size'] >= $maxsize)) 	    {
	        		$_SESSION['msg']	=	 "Sorry, File too large. File must be less than 8 megabytes.";
	        		$success2			=	1;
				    header("location:index.php");
	    		}
				else{
						$tableEditQry	=  "SELECT brochure						  
											FROM ".TABLE_PRODUCTS."		
											WHERE ".TABLE_PRODUCTS.".id='$editId'";
							
							$tableEdit 	=	mysql_query($tableEditQry);
							$editRow	=	mysql_fetch_array($tableEdit); 					
							$delete_brochure = $editRow['brochure'];
							
					$res=move_uploaded_file($_FILES["brochure"]["tmp_name"], $target_file);
					if ($res) {
						$data['brochure']	=	$newfilename;
						$success1			=	$db->query_update(TABLE_PRODUCTS,$data," id='{$editId}'");
						if($delete_brochure!='')
						{
							$file= ("brochure/" .$delete_brochure);
 							unlink($file);	 
						}						
				   	}
				}
			}
			else
			{
				$success1			=	$db->query_update(TABLE_PRODUCTS,$data," id='{$editId}'");
			}				
			$db->close();			
			if($success1)
			{							
				$_SESSION['msg']=	"Product Updated Successfully";											$success2			=	1;
			}
			else if($success2==0)
			{							
				$_SESSION['msg']="Failed";										
			}			
			header("location:index.php");
		}
		break;	
			
	case 'delete':
	
			$id = $_REQUEST['id'];	
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);	
			$db->connect();	
			// Deleting old image
			$tableEditQry	=  "SELECT brochure						  
								FROM ".TABLE_PRODUCTS."						  
								WHERE ".TABLE_PRODUCTS.".id='$id'";
			$tableEdit 	=	mysql_query($tableEditQry);
			$editRow	=	mysql_fetch_array($tableEdit); 
			
			$delete_brochure = $editRow['brochure'];
			$file= ("brochure/".$delete_brochure);
 			unlink($file);
 			
			$delete_image = $editRow['image'];
			$fileimage= ("productimage/".$delete_image);
 			unlink($fileimage);
			$db->query("DELETE 
						FROM `".TABLE_IMAGES."` 
						WHERE productID='{$id}'");	
			$success1	=	$db->query("DELETE 
										FROM `".TABLE_PRODUCTS."` 
										WHERE id='{$id}'");
			$db->close();								
			header("location:index.php");			
		break;
	case 'imageupload':	
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);	
			$db->connect();
			$data['productID'] = $_REQUEST['prodctID'];
			            
			$productimage 	   = $_FILES['productimage']['name'];
			$ext 			   = pathinfo($productimage, PATHINFO_EXTENSION);
			$next			   = date("ymdhis");
			$imagePath 		   = 'product'.$next.'.'.$ext;
			
			$allowedExt 	   = array('jpeg','jpg','jif','png');
			if(in_array($ext, $allowedExt)){
				$sourcePath    = $_FILES['productimage']['tmp_name'];
				$targetPath    = "productimage/" .$imagePath;
				$data['image'] = $imagePath;
				move_uploaded_file($sourcePath, $targetPath);
			}
			$success1=$db->query_insert(TABLE_IMAGES,$data);			
			$db->close();
			if($success1)
				{							
					$_SESSION['msg']= "Updated Successfully";		
				}
				else{
					$_SESSION['msg']="Failed";
				}		
			header("location:index.php");
		break;	
	case 'deleteimage':
			
			//$productId = $_REQUEST['productId'];
			$id = $_REQUEST['id'];	
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);	
			$db->connect();	
			// Deleting old image
							
					$tableEditQry	=  "SELECT *						  
										FROM ".TABLE_IMAGES."						  
										WHERE ".TABLE_IMAGES.".id='$id'";
					
					$tableEdit 	=	mysql_query($tableEditQry);
					$editRow	=	mysql_fetch_array($tableEdit); 
											
			// End
			$delete_image = $editRow['image'];
							
			$file= ("productimage/".$delete_image);
 			unlink($file);
			
			$db->query("DELETE FROM `".TABLE_IMAGES."` WHERE id='{$id}'");								
			$db->close(); 
			
			$_SESSION['msg']="Deleted Successfully";					
			header("location:index.php");
					    
}
?>