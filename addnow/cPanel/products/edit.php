<?php 
$page_id=3;
include("../header.php"); ?>
<?php
if(($_SESSION['LogID'])==''||($_SESSION['LogType']!='admin'))
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

    $editId=$_REQUEST['id'];
	$tableEditQry	=  "SELECT *						  
						FROM ".TABLE_PRODUCTS."						  
						WHERE ".TABLE_PRODUCTS.".id='$editId'";
						$selectAll = "SELECT ".TABLE_PRODUCTS.".id,
										   ".TABLE_PRODUCTS.".categoryID,
										   ".TABLE_PRODUCTS.".productName,
										   ".TABLE_PRODUCTS.".productDescription,
										   ".TABLE_PRODUCTS.".brochure,
										   ".TABLE_CATEGORY.".categoryName
										FROM
										   ".TABLE_PRODUCTS.",
										   ".TABLE_CATEGORY." 
										WHERE
										   ".TABLE_PRODUCTS.".id = $editId ";
	
	$tableEdit 	=	mysql_query($tableEditQry);
	$editRow	=	mysql_fetch_array($tableEdit);
	//$catId		=	$editRow['categoryId'];

?>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>

<div id="page-wrapper">
          <div class="container-fluid">
          
              <form action="do.php?op=edit" class="form1" method="post" enctype="multipart/form-data"> <!-- onsubmit="return valid()" -->
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>"> 		  
               
			  <div class="row">
				   <div class="col-lg-12">
                    <?php
					if(isset($_SESSION['msg']))
					{
						if($_SESSION['msg']=='')
						{
						?>
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:none">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['msg']; ?>
                            </div>
                        <?php 
						} 
						else
						{
						?>
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:block">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['msg']; ?>
                            </div>
						<?php 
						} ?>
                        
                    <?php 
					}	
					$_SESSION['msg']='';
					?>
                        <h1 class="page-header">
                          <small>Edit Products</small> <a href="index.php" style="float:right" class="btn btn-primary"> < Back </a>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-fw fa-table"></i>  <a href="index.php">Products</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i>Edit Products
                            </li>
                        </ol>
                    </div>                  
                    <div class="col-lg-6" style="float:none !important;margin:0 auto;">
                    
                    		<div class="form-group">
                                <label>Prodcut Name</label>
                                <input type="text" id="productName" name="productName" class="form-control" required="" value="<?= $editRow['productName']; ?>">
                            </div>
                            
                            <div class="form-group">
                            	<label>Select Category</label>
                             <select name="category" class="admin_inner" id="category" >
                            	<?php
		                      	$select = mysql_query("select * from ".TABLE_CATEGORY."");
		                      	while($row=mysql_fetch_array($select))
		                      	{
								?>
								<option value="<?php echo $row['id'];?>" <?= ($row['id'] == $editRow['categoryID']) ? "selected" : "" ?>><?php echo $row['categoryName'];?></option>
								<?php
								}
		                      	?>
                            </select>
                            </div> 
                                                       
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" style="max-width:100%;min-height:145px;" class="form-control" rows="3"><?= $editRow['productDescription']; ?></textarea>
                            </div>
                            
                            <div class="form-group">
                                <label>Upload Brochure</label>
                                <input name="brochure" type="file" value="">
                            </div>  
                            
                            <input style="float:right;" type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
                            
                      </div>                    
             	 	</div>
                 </form>                 
             </div>
         </div>      
  	  <div>
  </div> 
      <!-- jQuery -->
    <script src="../../js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.min.js"></script>
    <!-- Morris Charts JavaScript -->
    
    <script src="../../js/plugins/morris/raphael.min.js"></script>
    <script src="../../js/plugins/morris/morris.min.js"></script>
    <script src="../../js/plugins/morris/morris-data.js"></script>
</body>
</html>