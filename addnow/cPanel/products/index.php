<?php 
$page_id=1;
include("../header.php"); ?>
<?php
if(($_SESSION['LogID'])==''||($_SESSION['LogType']!='admin'))
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
        <!--end header-->
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    <?php
					if(isset($_SESSION['msg']))
					{
						if($_SESSION['msg']=='')
						{
						?>
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:none">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['msg']; ?>
                            </div>
                        <?php 
						} 
						else
						{
						?>
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:block">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['msg']; ?>
                            </div>
						<?php 
						} ?>
                        
                    <?php 
					}	
					$_SESSION['msg']='';
					?>
                        <h1 class="page-header">
                          <small> PRODUCTS </small> <a href="#" data-toggle="modal" data-target="#myModal1" style="float:right" class="btn btn-primary">Add New</a>
                          <a href="#" data-toggle="modal" data-target="#myModal2"  style="float:right; margin-right:10px" class="btn btn-primary">View Category</a>
                        </h1>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover offer_table">
                                <thead>
                                    <tr>
                                    	<th>SLNO</th>
                                    	<th>Product Name</th>
                                        <th>Category</th>
                                        <th>Description</th>
                                        <th>Brochure</th>
                                        <th width="55">Image</th>
                                        <th width="110">Edit / Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
									$i=1;
									$selectAll = "SELECT
												   ".TABLE_PRODUCTS.".id,
												   ".TABLE_PRODUCTS.".productName,
												   ".TABLE_PRODUCTS.".productDescription,
												   ".TABLE_PRODUCTS.".brochure,
												   ".TABLE_CATEGORY.".categoryName,
												   ".TABLE_CATEGORY.".description
												FROM
												   ".TABLE_PRODUCTS.",
												   ".TABLE_CATEGORY." 
												WHERE
												   ".TABLE_PRODUCTS.".categoryID = ".TABLE_CATEGORY.".id ";
									$result = $db->query($selectAll);
									$number = mysql_num_rows($result);
									if($number==0)
									{
									?>
                                        <tr><td colspan="7" align="center">There is no data in list. </td></tr>
									<?php
									}
									else
									{
										/*********************** for pagination ******************/
										$rowsPerPage = ROWS_PER_PAGE;
										if(isset($_GET['page']))
										{
											$pageNum = $_GET['page'];
										}
										else
										{
											$pageNum =1;
										}
										$offset = ($pageNum - 1) * $rowsPerPage;
										$select2=$db->query($selectAll." limit $offset, $rowsPerPage");
										//echo $select2;
										$i=$offset+1;
										//use '$select1' for fetching
										/*************************** for pagination **************/
										while ($row = mysql_fetch_array($select2)) 
										{
											$productId = $row['id'];
										?>
										<tr>
											<td><?= $i; ?></td>
											<td><?= $row['productName']; ?> </td>
											<td><?= $row['categoryName']; ?></td>
											<td><?= $row['productDescription']; ?></td>
                                            <td><?php if($row['brochure']){ ?><a target="_blank" href="brochure/<?= $row['brochure']; ?>">Click here</a><?php } ?></td>
											<td>
                                            	<a href="#" style="display: inline-block;" data-target="#myModalimage<?= $i; ?>" data-toggle="modal" class="btn btn-primary">view</a>
                                                    <!-- image view-->
                                                <div class="modal fade" id="myModalimage<?= $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                      <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <button type="button" style="margin-top:-9px; outline:none" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                           <h4 class="modal-title" id="myModalLabel">Upload Image</h4>
                                                          </div>
                                                          <div class="modal-body">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style=" height:auto;margin-top:5px; padding:0;">                                                    <?php
                                                                $tableimage	=  "SELECT *						  
												                                    FROM ".TABLE_IMAGES."						  
												                                    WHERE ".TABLE_IMAGES.".productID='$productId'";
																					
																$resultimage = $db->query($tableimage);
									
																$numberimage = mysql_num_rows($resultimage);
																if($numberimage==0)
																{
																?>
																	<p>Here is no data in list.</p>
																<?php
																}
																else
																{
																	while ($rowimage = mysql_fetch_array($resultimage)) 
										                            {
																	?>
                                                                    
																	<div style=" width:30%; float:left; margin-right:15px; position:relative; margin-bottom:20px">
                                                                    	<img style="width:100%; height:100px" src="productimage/<?= $rowimage['image']; ?>" />
                                                                        <a href="do.php?op=deleteimage&id=<?= $rowimage['id']; ?>" style="margin-top:-9px;outline:none;width:20px;height:20px;background:#F00 !important;color:#fff;border:#F00 !important;position:absolute;top:0;right:-9px;border-radius:20px;z-index:999999;display:block; text-align:center; cursor:pointer"><span aria-hidden="true" onclick="return delete_type();">&times;</span></a>
                                                                    </div>

                                                                    <?php
																	}
																}
																	?>
                                                                    
																	<form style="width:100%; float:left" role="form" method="post" enctype="multipart/form-data" action="do.php?op=imageupload&prodctID=<?= $productId ?>">
                                                                    	<div class="form-group">
                                                                     		<label>Upload image (768 x 785pixels)</label>
                                                                     		<input name="productimage" type="file" required="required">
                                                                		</div>  
                                                                       <button style="float:right" type="submit" class="btn btn-primary" name="submit">Upload</button>
																	</form>
                                                             
                                                               
                                                                    <!--<img style="width:100%" src="">-->
                                                                </div>
                                                          </div>
                                                          <div class="modal-footer" style="display: inline-block;width: 100%;">
                                                          </div>
                                                        </div>
                                                      </div>
                                                      
                                                </div>
    <!-- image view-->
                                                
                                                
                                                
                                            </td>
                                            

											<td>
											<a onclick="return confirm('Do you want to delete this product?');" href="do.php?op=delete&id=<?= $productId ?>" style="float:right;" class="btn btn-primary"><i class="fa fa-remove"></i></a>
											<a href="edit.php?op=edit&id=<?= $productId ?>" style="float:right;margin-right:10px" class="btn btn-primary"><i class="fa fa-edit"></i></a>
											</td>
										</tr>
										<?php
										$i++;
								   		}
									}
                    			?>
                                </tbody>
                            </table>
                        </div>
                        
                        <!-- paging -->		
            <!--<div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div> -->       
            <!-- paging end-->
            
            <!--*****************************************************************-->
            
            	 <?php 
                  if($number>@$rowsPerPage)
					{
					?>	
					 <br />	
					  <div class="pagerSC" align="center">
					<?php
					
					$query   =  $db->query($selectAll);
					$numrows = mysql_num_rows($query);
					$maxPage = ceil($numrows/$rowsPerPage);
					$self = $_SERVER['PHP_SELF'];
					$nav  = '';
					if ($pageNum - 5 < 1) {
					$pagemin = 1;
					} else {
					$pagemin = $pageNum - 5;
					};
					if ($pageNum + 5 > $maxPage) {
					$pagemax = $maxPage;
					} else {
					$pagemax = $pageNum + 5;
					};
					
					for($page = $pagemin; $page <= $pagemax; $page++)
					{
					   if ($page == $pageNum)
					   {
						  $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
					   }
					   else
					   {
						 	if(@$search)
					   		 {
							 	$nav .= " <a href=\"$self?page=$page&sname=$search\">$page</a> ";
							 }
							 else
							 {
							 	$nav .= " <a href=\"$self?page=$page\">$page</a> ";
							 }
					   }
					}
					?>
					 <?php
					if ($pageNum > 1)
					{
					   $page  = $pageNum - 1;
					   if(@$search)
					   {
						   $prev  = " <a href=\"$self?page=$page&sname=$search\">Prev</a> ";
						   $first = " <a href=\"$self?page=1&sname=$search\">First Page</a> ";
					   }
					   else
					   {
						   $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
						   $first = " <a href=\"$self?page=1\">First Page</a> ";
					   }
					}
					else
					{
					   $prev  = '&nbsp;';
					   $first = '&nbsp;';
					}
					
					if ($pageNum < $maxPage)
					{
					   $page = $pageNum + 1;
					   if(@$search)
					   {
						   	 $next = " <a href=\"$self?page=$page&sname=$search\">Next</a> ";
						     $last = " <a href=\"$self?page=$maxPage&sname=$search\">Last Page</a> ";
					   }
					   else
					   {
						   	 $next = " <a href=\"$self?page=$page\">Next</a> ";
						   	 $last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
					   }
					}
					else
					{
					   $next = '&nbsp;';
					   $last = '&nbsp;';
					}
					echo $first . $prev . $nav . $next . $last;
					?>
					<div style="clear: left;"></div>
					</div>	 
				<?php
				}
                ?>
            
           <!-- ******************************************************************-->
                        
                        
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
    </div>
    <!-- add new_ideas-->
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow:auto;padding-bottom:20px">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Products</h4>
              </div>
              <div class="modal-body">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 Enqry" style="height:auto;margin-top:5px; padding:0;">
                    
                    <form role="form" method="post" enctype="multipart/form-data" action="do.php?op=index">
                    <div style="float:none !important;margin:0 auto;">
                            <div class="form-group">
                                <label>Prodcut Name</label>
                                <input type="text" id="productName" name="productName" class="form-control" required="">
                            </div>                            
                            <div class="form-group">
                            	<label>Select Category</label>
                             <select name="category" class="admin_inner" id="category" >
                            	<?php
		                      	$select = mysql_query("select * from ".TABLE_CATEGORY."");
		                      	while($row=mysql_fetch_array($select))
		                      	{
								?>
								<option value="<?php echo $row['id'];?>"><?php echo $row['categoryName'];?></option>
								<?php
								}
		                      	?>
                            </select>
                            </div>
                            <!--for Ajax of sub category -->   
                             
							<!--end-->
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" style="max-width:100%;min-height:145px;" class="form-control" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Upload Brochure</label>
                                <input name="brochure" type="file">
                            </div>  
                            <button style="float:right" type="submit" class="btn btn-default" name="submit">Submit</button>
                            <button style="float:right; margin-right:10px" type="reset" class="btn btn-default">Reset</button>
                    </div>                    
                    </form>
                    </div>
              </div>
            </div>
          </div>
    </div>
    <!-- Ends add new_ideas-->

	<!--view Category-->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow:auto;padding-bottom:20px">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Product Categories</h4>
              </div>
              <div class="modal-body">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 Enqry" style="height:auto;margin-top:5px; padding:0;">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover offer_table">
                                <thead>
                                    <tr>
                                    	<th>SLNO</th>
                                    	<th>Product Category</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
								$i=1;
		                      	$select = mysql_query("select * from ".TABLE_CATEGORY."");
		                      	while($row=mysql_fetch_array($select))
		                      	{
								?>
                                
										<tr>
											<td><?= $i++; ?></td>
											<td><?php echo $row['categoryName'];?></td>
                                        </tr>
                                 <?php
								}
		                      	?>

                    </div>
              </div>
            </div>
          </div>
    </div>
    
    





    
    <!-- jQuery -->
    <script src="../../js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.min.js"></script>
    <!-- Morris Charts JavaScript -->
    
   <!-- <script>
		$(document).ready(function() {
            // Dynamic images in pop up
			$(document).on('click', '.offer_table > tbody > tr > td a[data-src]', function (e) {
				e.preventDefault();
				var imgSrc = $(this).attr('data-src');
				$('#myModal').find('.modal-body img').attr('src', '../Products/productPhoto/' + imgSrc);		
				$('#myModal').modal('show');	
			});
			
        });
	</script>-->
    
    
    <script src="../../js/plugins/morris/raphael.min.js"></script>
    <script src="../../js/plugins/morris/morris.min.js"></script>
    <script src="../../js/plugins/morris/morris-data.js"></script>
    <script> 
//delete row in index page
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
</body>
</html>
