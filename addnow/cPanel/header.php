<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
$loginId= $_SESSION['LogID'];
//$loginType= $_SESSION['LogType'];
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Altron Admin</title>
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/sb-admin.css" rel="stylesheet">
    <link href="../../css/plugins/morris.css" rel="stylesheet">
    <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <!--start header-->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href=""><img style="width:200px;margin-top: -4px;" src="../../images/logoinner.png"></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="../changePassword/index.php"><i class="fa fa-fw fa-gear"></i> Change Password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
            
            	
                <ul class="nav navbar-nav side-nav">
                
                    <!--<li class="active">
                        <a href="../category/index.php"><i class="fa fa-fw fa-table"></i>Category</a>
                    </li>-->
                    <!--<li >
                        <a href="../subCategory/index.php"><i class="fa fa-fw fa-table"></i>Sub Category</a>
                    </li>-->
                    <li <?php if($page_id==1) echo'class="active"'; ?>>
                        <a href="../products/index.php"><i class="fa fa-fw fa-table"></i>Products</a>
                    </li>
<!--                    <li  <?php //if($page_id==2) echo'class="active"';?>>
                        <a href="../gallery/index.php"><i class="fa fa-fw fa-edit"></i>Gallery</a>
                    </li> 
-->                                       
                </ul>
               
                
            </div>
            <!-- /.navbar-collapse -->
        </nav>