<?php
 $page_id=1;
 include('includes/header.php'); ?>

<?php include('includes/menu.php'); ?>
    <div class="banner_wrap">
        <ul class="banner">
            <li>
                <div class="banner_box banner_box1">
                    <div class="container">
                        <div class="cap_wrap">
                            <div class="cap_block">
                                <h3>Save Today. Survive Tomorrow</h3>
                                <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam volutpat purus in<br />ferment uectetur tortor</p>-->
                            </div>
                        </div>
                        <div class="banner_inner">                            
                            <img src="images/1.png" alt="" />
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="banner_box banner_box2">
                    <div class="container">
                        <div class="cap_wrap">
                            <div class="cap_block">
                                <h3>Conserve to Preserve</h3>
                                <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam volutpat purus in<br />ferment uectetur tortor</p>-->
                            </div>
                        </div>
                        <div class="banner_inner">                            
                            <img src="images/2.png" alt="" />
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="banner_box banner_box1">
                    <div class="container">
                        <div class="cap_wrap">
                            <div class="cap_block">
                                <h3>We Bring Good Things To Life</h3>
                                <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam volutpat purus in<br />ferment uectetur tortor</p>-->
                            </div>
                        </div>
                        <div class="banner_inner">                            
                            <img src="images/3.png" alt="" />
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="banner_box banner_box2">
                    <div class="container">
                        <div class="cap_wrap">
                            <div class="cap_block">
                                <h3>The Perfect Experience</h3>
                                <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam volutpat purus in<br />ferment uectetur tortor</p>-->
                            </div>
                        </div>
                        <div class="banner_inner">                            
                            <img src="images/4.png" alt="" />
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    
    <!-- Welcome -->
    <div class="container welcome_wrap">
        <div class="welcome_head">
            <h3>welcome to</h3>
            <img src="images/br_logo.png" alt="Altron" />
            <h3>Digital Systems</h3>
        </div>
        <p class="welcome_text">Altron digital systems Pvt. Ltd. stand as the perfect solution makers in the field of
 power electronics. We offer an extensive range of products including various solar products, UPS
 and customized solar solutions. Altron's strength is an outstanding R&amp;D to achieve compact high quality products along with 
 a dedicated team of technical experts .A well experienced management team ensures timely delivery of products.</p>
        <div class="wel_icos">
            <div class="wel_ico_block">
                <span class="wel_ico" style="background-image: url(images/wel_ico1.png)"></span>
                <h4>Best in quality</h4>
            </div>
            <div class="wel_line">
                <span class="left_disk"></span>
                <span class="right_disk"></span>
            </div>
            <div class="wel_ico_block">
                <span class="wel_ico" style="background-image: url(images/wel_ico2.png)"></span>
                <h4>Energy efficiency</h4>
            </div>
            <div class="wel_line">
                <span class="left_disk"></span>
                <span class="right_disk"></span>
            </div>
            <div class="wel_ico_block">
                <span class="wel_ico" style="background-image: url(images/wel_ico3.png)"></span>
                <h4>Eco friendly</h4>
            </div>
        </div>
    </div>
    <!-- /Welcome -->
    
    <!-- Products -->
    <div class="product_wrap" style="margin: 70px 0px 0;">
        <div class="container">
            <div class="product_head">
                <h3>products</h3>
                <span><i class="fa fa-cog"></i></span>
            </div>
        </div>
        <div class="container pro_bg">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pro_list_rowa">
                        <div class="pro_list_inner">
                            <img src="images/ups.png" alt="" />
                            <h4>UPS</h4>
                            <p>An uninterruptible power supply(UPS) provides emergency power to a load when input mains power fails... </p>
                            <a href="product.php">read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pro_rowb">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="pro_list_rowb_left">
                        <div class="pro_list_inner">
                            <img src="images/homeups.png" alt="" />
                            <h4>home ups</h4>
                            <p>Altron HOME UPS series are specially designed for household appliances.  The latest DSP based technology...</p>
                            <a href="product.php">read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
                    <div class="pro_list_rowb_right">
                        <div class="pro_list_inner">
                            <img style="margin:-62px auto 0px auto;" src="images/solar.png" alt="" />
                            <h4>SOLAR PCU</h4>
                            <p>Solar Power Conditioning Unit (SOLAR PCU) consists inverter,grid charger and an integrated solar charge controller...</p>
                            <a href="product.php">read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pro_rowc">
                <div class="col-lg-12">
                    <div class="pro_list_rowc">
                        <div class="pro_list_inner">
                            <img style="margin-top:-215px;" src="images/solarchrage.png" alt="" />
                            <h4>SOLAR CHARGE CONTROLLERS</h4>
                            <p>Our range of solar charge controllers are designed with intelligent load / Dusk to dawn control options...</p>
                            <a href="product.php">read more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container res_pro">
            <div class="row">
                <div class="col-sm-6">
                    <div class="res_pro_list">
                        <div class="res_pro_img"><img src="images/ups.png" alt="" /></div>
                            <h4>UPS</h4>
                            <p>An uninterruptible power supply(UPS) provides emergency power to a load when input mains power fails... </p>
                            <a href="product.php">read more</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="res_pro_list">
                        <div class="res_pro_img"><img src="images/homeups.png" alt="" /></div>
                            <h4>home ups</h4>
                            <p>Altron HOME UPS series are specially designed for household appliances.  The latest DSP based technology...</p>
                            <a href="product.php">read more</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="res_pro_list res">
                        <div class="res_pro_img"><img src="images/solar.png" alt="" /></div>
                            <h4>SOLAR PCU</h4>
                            <p>Solar Power Conditioning Unit (SOLAR PCU) consists inverter,grid charger and an integrated solar charge controller...</p>
                            <a href="product.php">read more</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="res_pro_list">
                        <div class="res_pro_img"><img src="images/solarchrage.png" alt="" /></div>
                            <h4>SOLAR CHARGE CONTROLLERS</h4>
                            <p>Our range of solar charge controllers are designed with intelligent load / Dusk to dawn control options...</p>
                            <a href="product.php">read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Products -->
    
    <!-- Testimonial --
    <div class="container testimonial_wrap">
        <div class="testimonial_head">
            <h3>testimonial</h3>
            <span><img src="images/testi_img.png" alt="" /></span>
        </div>
        <div class="testimonial_inner">
            <span class="quote_left quotes"><i class="fa fa-quote-left"></i></span>
            <span class="quote_right quotes"><i class="fa fa-quote-right"></i></span>
            <ul class="testimonial">
                <li>
                    <div class="testi_inner">
                        <div class="testimonial_img">
                            <img src="images/testi1.png" alt="" />
                        </div>
                        <h4>John Luis</h4>
                        <p class="designation">From Galaxy Powers</p>
                        <p class="testimonial_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis venenatis.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus</p>
                    </div>
                </li>
                <li>
                    <div class="testi_inner">
                        <div class="testimonial_img">
                            <img src="images/testi1.png" alt="" />
                        </div>
                        <h4>John Luis</h4>
                        <p class="designation">From Galaxy Powers</p>
                        <p class="testimonial_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis venenatis.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus</p>
                    </div>
                </li>
                <li>
                    <div class="testi_inner">
                        <div class="testimonial_img">
                            <img src="images/testi1.png" alt="" />
                        </div>
                        <h4>John Luis</h4>
                        <p class="designation">From Galaxy Powers</p>
                        <p class="testimonial_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis venenatis.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- /Testimnial -->
<?php include('includes/footer.php'); ?>