<?php
 $page_id=4;
 include('includes/header.php'); ?>

<?php include('includes/menu.php'); ?>
    <div class="contact_wrap">
        <form action="contact-mail.php" method="post">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="product_head contact_head">
                            <h3>COntact Us</h3>
                            <span><i class="fa fa-cog"></i></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="add_block">
                            <span><i class="fa fa-map-marker"></i>&nbsp;Platinum Tower, Pavamani Road, Calicut</span> 
                            <span><i class="fa fa-phone"></i>&nbsp;<a href="tel:1234678910">0495-3204426</a></span> 
                            <span><i class="fa fa-at"></i>&nbsp;<a href="mailto:info@gmail.com">info@altron.com</a></span> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="map_head">
                            <h4>Get in touch with us</h4>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
	                    
	                        <div class="form_block">
	                            <input type="text" name="name" class="valid_entry" placeholder="Name" required />
	                        </div>
	                        <div class="form_block">
	                            <input type="text" name="contact_number" class="valid_entry" placeholder="Contact number" />
	                        </div>
	                        <div class="form_block">
	                            <input type="email" name="email" class="valid_entry" placeholder="Email" required />
	                        </div>
	                    </div>
	                    <div class="col-lg-6 col-md-6 col-sm-6">
	                    	<div class="form_block">
	                    	    <input name="subject" class="valid_entry" placeholder="Subject " type="text">
	                    	</div>
	                        <div class="form_block">
	                            <textarea name="message" class="valid_entry" placeholder="Message..." required></textarea>
	                        </div>
	                        <div class="form_block text-right">
	                            <input name="submit" value="Send" type="submit">/>
	                        </div>
	                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="gmap"></div>
                    </div>
                </div>
            </div> 
        </form>
    </div>
<?php include('includes/footer.php'); ?>